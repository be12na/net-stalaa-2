<?php

namespace App\Http\Controllers\Main;

use App\Helpers\Neo;
use App\Models\Branch;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

trait DashboardTrait
{
    protected function mainDashboard(Request $request, Neo $neo)
    {
        if ($request->ajax()) {
            return $this->mainDashboardSummary($request, $neo);
        }

        $monthTitle = formatDatetime(Carbon::today(), 'F Y');
        // $products = Product::query()->byActive()->byPublished()->orderBy('package_range')->get();

        return view('main.dashboard', [
            'monthTitle' => $monthTitle,
            'windowTitle' => 'Dashboard',
            'breadcrumbs' => ['Dashboard'],
            // 'products' => $products,
        ]);
    }

    private function mainDashboardSummary(Request $request, Neo $neo)
    {
        $branchIds = Branch::byActive()->get()->pluck('id')->toArray();
        $scope = $request->get('s', 'member'); // member, mitra
        $result = null;
        // $formattedCurrency = true;

        if ($scope == 'member') {
            $data = $request->get('d', 'sale'); // distributor, agent, mitra, sale
            if ($data == 'sale') {
                $timeSale = $request->get('t', 'current'); // current, last, month
                $value = 0;
                $saleScope = ['scope' => 'member'];
                if ($timeSale == 'current') {
                    $value = $neo->sumOfThisWeekSales($branchIds, $saleScope);
                } elseif ($timeSale == 'last') {
                    $value = $neo->sumOfLastWeekSales($branchIds, $saleScope);
                } elseif ($timeSale == 'month') {
                    $value = $neo->sumOfThisMonthSales($branchIds, $saleScope);
                }

                $result = new HtmlString(formatCurrency($value, 0, true, false));
            }
        } elseif ($scope == 'mitra') {
            $data = $request->get('d', 'summary');

            if ($data == 'omzet') {
                $timeSale = $request->get('t', 'current'); // current, last, month
                $value = 0;
                $saleScope = ['scope' => 'mitra'];
                if ($timeSale == 'current') {
                    $value = $neo->sumOfThisWeekSales($branchIds, $saleScope);
                } elseif ($timeSale == 'last') {
                    $value = $neo->sumOfLastWeekSales($branchIds, $saleScope);
                } elseif ($timeSale == 'month') {
                    $value = $neo->sumOfThisMonthSales($branchIds, $saleScope);
                }

                $result = new HtmlString(formatCurrency($value, 0, true, false));
            } elseif ($data == 'summary') {
                $typeSum = $request->get('t', 'reseller');

                if ($typeSum == 'distributor') {
                    $result = 0; //formatNumber($neo->countDistributor(), 0);
                } elseif ($typeSum == 'agent') {
                    $result = 0; //formatNumber($neo->countAgent(), 0);
                } elseif ($typeSum == 'reseller') {
                    $result = formatNumber($neo->countReseller(), 0);
                } else {
                    $result = formatNumber($this->countMitra($typeSum, $request->get('p')), 0);
                }
            }
        }

        $status = is_null($result) ? 404 : 200;

        return response($result, $status);
    }

    private function countMitra(string $typeSum, int $typeId = null): int
    {
        $query = User::query()->byMitraGroup()->byActivated();

        if ($typeSum == 'month') {
            $dateYm = date('Y-m');
            $query = $query->whereRaw("DATE_FORMAT(activated_at, '%Y-%m') = '{$dateYm}'");
        } elseif ($typeSum == 'today') {
            $dateYmd = date('Y-m-d');
            $query = $query->whereRaw("DATE_FORMAT(activated_at, '%Y-%m-%d') = '{$dateYmd}'");
        } elseif ($typeSum == 'mitra') {
            if (is_null($typeId)) $typeId = MITRA_TYPE_DROPSHIPPER;

            $query = $query->byMitraType($typeId);
        } else {
            $query = $query->where('id', '=', 0);
        }

        return $query->count();
    }
}
