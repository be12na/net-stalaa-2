<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixMitraType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fix-mitra-type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Memperbaikin jenis mitra antara Dropshipper, Reseller dan Agen';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::query()
            ->byMitraGroup()
            ->where(function ($where) {
                return $where
                    ->whereHas('completeShoppings', function ($mitraPurchase) {
                        return $mitraPurchase->whereHas('products', function ($mitraPurchaseProduct) {
                            return $mitraPurchaseProduct->whereIn('product_id', [1, 2, 3, 6]);
                        });
                    })
                    ->orWhere('id', '=', 5);
            })
            ->whereNot('mitra_type', MITRA_TYPE_AGENT)
            ->get();

        $countUser = $users->count();
        $userIds = $users->pluck('id')->toArray();

        if ($countUser > 0) {
            DB::table('users')
                ->whereIn('id', $userIds)
                ->update([
                    'mitra_type' => MITRA_TYPE_AGENT,
                    'level_id' => MITRA_LEVEL_AGENT,
                ]);

            $this->info("Updated: {$countUser}");
        } else {
            $this->warn('Tidak ada mitra yang diupdate');
        }

        return Command::SUCCESS;
    }
}
