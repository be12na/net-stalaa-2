function expandDownlines(row, url)
{
    if (!row.hasClass('loaded')) {
        $.get({
            url: url,
            data: {
                'root_id': row.data('root-id'),
                'parent_id': row.data('member-id'),
            },
        }).done(function(response) {
            row.addClass('expanded loaded').after(response);
        });
    } else {
        const target = row.data('children');
        const isExpanded = row.hasClass('expanded');
        if (isExpanded) {
            $('tr' + target).addClass('d-none');
        } else {
            let cps = [];
            $('tr' + target).each(function() {
                const tr = $(this);
                if (!tr.hasClass('expanded')) {
                    cps.push(tr);
                }
                tr.removeClass('d-none');
            });
            if (cps.length > 0) {
                $.each(cps, function(index, elem) {
                    $('tr' + elem.data('children')).addClass('d-none');
                });
            }
        }
        row.toggleClass('expanded');
    }
}

$(function() {
    const targetTable = 'table.table-tree[data-child-url]';
    const targetNode = targetTable + '> tbody > tr:not(.no-children) > .cell-tree-node';

    $(document).on('click', function(e) {
        const target = $(e.target);
        let row = false;
        const isTarget = (target.is(targetNode) || target.is(targetNode + ' *'));

        if (isTarget) {
            const table = target.closest(targetTable);

            if (table.length) {
                row = target.closest('tr');
                expandDownlines(row, table.data('child-url'));
            }
        }

    });

    $(targetNode).trigger('click');
});
